# CSCM38-CNNDDoS

### 951536

This project is for the CSCM38 module at Swansea University. It examines using a CNN based network to identify potential DDoS attacks from the CIC-DDoS2019 dataset.

The full repository with the complete dataset can be found at https://gitlab.com/EllisDev/cscm38-cnnddos.

The zipped version of this project contains a smaller dataset ('Simple Data' from the main repository) of 30000 samples (5000 of each type). The models were trained on a larger dataset so results may not be reproducible on this smaller set.
